pragma solidity ^0.5.0;

contract Todo {
    // an array which holds all tasks
    Task[] private tasks;

    // Workstate enum which holds all states of a task
    enum WorkState {NONE, TODO, IN_PROGRESS, IN_REVIEW, DONE}

    // Mapping from owner to a list of owned tasks
    mapping(address => uint[]) private taskOwners;

    // Mapping from task of specific address
    mapping(address => uint[]) private todos;

    // Mapping from task of specific address
    mapping(uint => StateHistory[]) private history;

    // event trigger when new task created
    event TaskCreated(address _owner, uint _taskId);

    // Task struct which holds all the required info
    struct Task {
        uint256 id;
        string title;
        string description;
        string metadata;
        string[] tags;
        address owner;
        address doer;
        uint256 price;
        WorkState state;
        uint256 index;
    }

    // StateHistory struct which holds all the changes on state of specific task
    struct StateHistory {
        WorkState from;
        WorkState to;
        address by;
    }

    /**
    * @dev Guarantees msg.sender is owner of the given task
    * @param _taskId uint ID of the task to validate its ownership belongs to msg.sender
    */
    modifier isOwner(uint _taskId) {
        require(tasks[_taskId].owner == msg.sender, "You are not owner of this task");
        _;
    }
    /**
    * @dev Guarantees msg.sender is doer of the given task
    * @param _taskId uint ID of the task to validate its ownership belongs to msg.sender
    */
    modifier isDoer(uint _taskId) {
        require(tasks[_taskId].doer == msg.sender, "This task is not yours.");
        _;
    }
    /**
    * @dev Guarantees msg.sender is doer of the given task
    * @param _taskId uint ID of the task to validate its ownership belongs to msg.sender
    */
    modifier isOwnerOrDoer(uint _taskId) {
        require(
            tasks[_taskId].doer == msg.sender || tasks[_taskId].owner == msg.sender,
            "You have to owner or doer of the task to do this action."
        );
        _;
    }

    /**
    * @dev Guarantees msg.sender is doer of the given task
    * @param _taskId uint ID of the task to validate it has not any doer yet
    */
    modifier isTaskUnassigned(uint _taskId) {
        require(tasks[_taskId].doer == address(0),"Cannot re-assign task yet.");
        _;
    }

    /**
    * @dev Guarantees msg.sender is doer of the given task
    * @param _taskId uint ID of the task
    * @param _to WorkState destination state of the task
    */
    modifier isFitInState(uint _taskId, WorkState _to) {
        if (_to == WorkState.NONE) {
            require(_to == WorkState.TODO, "Workflow restricted");
        }
        else if (_to == WorkState.TODO) {
            require(_to == WorkState.IN_PROGRESS, "Workflow restricted");
        }
        else if (_to == WorkState.IN_PROGRESS) {
            require(_to == WorkState.IN_REVIEW, "Workflow restricted");
        }
        else if (_to == WorkState.IN_REVIEW) {
            require(_to == WorkState.DONE, "Workflow restricted");
        }
        _;
    }

    /**
    * @dev create new task (to-do)
    * @param title string is the title of the task
    * @param description string is the description of the task
    * @param metadata string is the searchable field of the task
    * @param price uint256 is the prize of doer in ETH
    * @return bool is the task created successfully
    */
    function createTask(
        string memory title,
        string memory description,
        string memory metadata,
        // string[] memory tags,
        uint256 price
    ) public returns(bool) {
        uint id = tasks.length;
        Task memory newTask;
        newTask.title = title;
        newTask.description = description;
        newTask.metadata = metadata;
        // newTask.tags = tags;
        newTask.price = price;
        newTask.doer = address(0);
        newTask.id = id;
        newTask.owner = msg.sender;
        newTask.state = WorkState.TODO;

        StateHistory memory newHistory;
        newHistory.from = WorkState.NONE;
        newHistory.to = WorkState.TODO;
        newHistory.by = msg.sender;

        history[id].push(newHistory);


        // TODO: assign task to creator at first
        tasks.push(newTask);
        taskOwners[msg.sender].push(id);
        emit TaskCreated(msg.sender, id);
        return true;
    }

    /**
    * @dev Guarantees msg.sender is doer of the given task
    * @param taskId uint ID of the task
    * @return uint ID the id of the task
    */
    function ownTask(uint taskId) public isTaskUnassigned(taskId) returns(uint) {
        Task memory task = tasks[taskId];
        todos[msg.sender].push(taskId);
        task.doer = msg.sender;
        return task.id;
    }

    function changeState(
        WorkState state,
        uint taskId
    ) public isOwnerOrDoer(taskId) isFitInState(taskId, state) returns(bool) {
        Task memory task = tasks[taskId];
        StateHistory memory newHistory;
        newHistory.from = task.state;
        newHistory.to = state;
        newHistory.by = msg.sender;
        history[taskId].push(newHistory);
        task.state = state;
        return true;
    }

    function deleteTask(uint taskId) public isOwner(taskId) returns(bool) {
        //TODO: Delete task if nobody take it
    }

    function doneTask(uint taskId) public isDoer(taskId) isFitInState(taskId, WorkState.DONE) returns(bool) {
        changeState(WorkState.DONE, taskId);
        //TODO: send prize to doer account
    }

    /**
    * @dev Disallow payments to this contract directly
    */
    function() external {
        revert("This is a contract account and does not approve any incoming payment directly.");
    }



}